var $ = jQuery.noConflict();

// Namespace - if the app namespace already exists then use the same object, otherwise create a new object
var app = app || {}; 
!function(){
	"use strict";

	$('html').removeClass('no-js');

	app.initialize = {
		init: function() {
			app.login.init();
		}
	},
	app.login = {
		init: function() {
			// Validate form before submit
			$('#login-form').submit(function(event) {
				// If the email AND password are both valid:
				if(app.validation.email(email) && app.validation.password(id)) {
					event.preventDefault(); // Prevent page from reloading
					document.write('Form has been submitted successfully'); // Confirmation message
				} 
				// If the email is valid but NOT the password:
				else if(app.validation.email(email) && !app.validation.password(id)) {
					event.preventDefault(); // Prevent page from reloading
					return app.errorMsg.password(id);
				} 				
				// If the password is valid but NOT the email:
				else if(!app.validation.email(email) && app.validation.password(id)) {
					event.preventDefault(); // Prevent page from reloading
					return app.errorMsg.email(id);
				}
				// If the email AND password are both invalid:
				else {
					event.preventDefault(); // Prevent page from reloading
					return app.errorMsg.email(id);
					return app.errorMsg.password(id);
				}     				
			});
		}
	},
	app.validation = {
		email: function(email) {
			// Regex, use this to validate the Email. It returns true or false.

			var re = /^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
			return re.test(email);
		},
		password: function(id) {
			// Use this to validate the Password. Checks if the value is empty. It returns true or false.

			var elementVal = $.trim(id.val());
			if(elementVal.length > 0) { // If the value is NOT empty
				return true;
			}
		}
	},
	app.errorMsg = {
		email: function(id) {
			var text = 'Your E-Mail is not valid';

			// Add error message in the form
			return id.text;
		},
		password: function(id) {
			var text = 'Your Password is not valid';

			// Add error message in the form
			return id.text;
		}
	}
	app.docOnReady = {
		init: function() {
			app.initialize.init();
		}
	};

	$(document).ready(app.docOnReady.init);

}(jQuery);