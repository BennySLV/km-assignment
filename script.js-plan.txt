script.js outline

Namespace (i.e. Global Object):
- app {...}

Sub-namespaces (i.e. Child objects):
- initialize {
	init()
}

- login {
	init()
}

- validation {
	email(email)
	password(id)
}

- errorMsg {
	email(email)
	password(id)
}

- docOnReady {
	init()
}